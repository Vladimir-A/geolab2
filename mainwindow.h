#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "bmpheaders.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

using namespace std;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    string nameOfColor2File = "color2.bmp";
    string nameOfColor16File = "color16.bmp";
    string nameOfColor256File = "color256.bmp";
    string nameOfColor16mFile = "color16m.bmp";

    void readRawMatrix(string location);
    BITMAPFILEHEADER createFileHeader(unsigned int);
    BITMAPV5HEADER createPV5Header(unsigned int);

    void createNewBMP();
    void createNewBMPcolor2(string);
    void createNewBMPcolor16(string);
    void createNewBMPcolor256(string);
    void createNewBMPcolor16m(string);


    DWORD Bitmap[480][640];
    BYTE *BitmapFull;
    size_t lengthOfReadFile;
    void openAndPrintBMP();
    void openAndPrintBMPcolor2();
    void openAndPrintBMPcolor16();
    void openAndPrintBMPcolor256();
    void openAndPrintBMPcolor16m();
    //void S8BitProc ();
};
#endif // MAINWINDOW_H
