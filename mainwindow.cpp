#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include "qimage.h"

#define ALING4(x) ((((x)-1)/4+1)*4) //Макрос - дополнение величины до ближайшей кратной 4

struct S4Bit                        //Структура для доступа к 4-битным изображениям
{
 BYTE b1:4;
 BYTE b2:4;
};

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    createNewBMP();
    openAndPrintBMP();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::readRawMatrix(string location)
{
    ifstream file;
    file.open(location,ios::binary|ios::in);

    file.seekg(0, std::ios::end);
    lengthOfReadFile = file.tellg();
    file.seekg(0, std::ios::beg);

    BitmapFull = new BYTE[lengthOfReadFile];
    if (file.is_open()) {
        file.read((char*)BitmapFull,lengthOfReadFile);
        file.close();
    }
}

BITMAPFILEHEADER MainWindow::createFileHeader(unsigned int offset)
{
    BITMAPFILEHEADER header;

    header.bfType = 0x4d42;
    header.bfSize = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPV5HEADER) + 0x4*offset + lengthOfReadFile;
    header.bfReserved1 = 0;
    header.bfReserved2 = 0;
    header.bfOffbits = 0x8A + 0x4*offset;

    return header;
}

BITMAPV5HEADER MainWindow::createPV5Header(unsigned int BitCount)
{
    BITMAPV5HEADER header;

    header.bV5Size = 0x7C;
    header.bV5Width = 640;
    header.bV5Height = 480;
    header.bV5Planes = 1;
    if (BitCount == 1){
        header.bV5BitCount = 1;
    }
    if (BitCount == 4){
        header.bV5BitCount = 4;
    }
    if (BitCount == 8){
        header.bV5BitCount = 8;
    }
    if (BitCount == 24){
        header.bV5BitCount = 24;
    }
    header.bV5Compression = 0;
    header.bV5SizeImage = lengthOfReadFile;
    header.bV5XPelsPerMeter = 160;
    header.bV5YPelsPerMeter = 160;
    header.bV5ClrUsed = 0;
    header.bV5ClrImportant = 0;
    header.bV5RedMask = 0x00FF0000;
    header.bV5GreenMask = 0x0000FF00;
    header.bV5BlueMask = 0x000000FF;
    header.bV5AlphaNask = 0xFF000000;
    header.bV5CSType = 0x73524742;     //'sRGB'

    //if 'sRGB' bV5Endpoints doesn't need
    //header.bV5Endpoints;
    header.bV5Endpoints.ciexyzRed.ciexyzX = 0.64;
    header.bV5Endpoints.ciexyzRed.ciexyzY = 0.33;
    header.bV5Endpoints.ciexyzRed.ciexyzZ = 0.03;
    header.bV5Endpoints.ciexyzGreen.ciexyzX = 0.30;
    header.bV5Endpoints.ciexyzGreen.ciexyzY = 0.60;
    header.bV5Endpoints.ciexyzGreen.ciexyzZ = 0.10;
    header.bV5Endpoints.ciexyzBlue.ciexyzX = 0.15;
    header.bV5Endpoints.ciexyzBlue.ciexyzY = 0.06;
    header.bV5Endpoints.ciexyzBlue.ciexyzZ = 0.79;


    header.bV5GammaRed = 0;
    header.bV5GammaGreen = 0;
    header.bV5GammaBlue = 0;

    header.bV5Intent = 4;
    header.bV5ProfileData = 0;
    header.bV5ProfileSize = 0;
    header.bV5Reserved = 0;

    return header;
}

void MainWindow::createNewBMP()
{
   //string path = "COLOR2.MTX";
   //string path = "COLOR16.MTX";
   //string path = "COLOR256.MTX";
   string path = "COLOR16M.MTX";

   createNewBMPcolor2(path);
   createNewBMPcolor16(path);
   createNewBMPcolor256(path);
   createNewBMPcolor16m(path);
}

void MainWindow::createNewBMPcolor2(string path)
{
    //заглушка
    //path = "COLOR2.MTX";

    readRawMatrix(path);

    BITMAPFILEHEADER bmfh = createFileHeader(2);
    BITMAPV5HEADER bmV5h = createPV5Header(1);

    RGBQUAD rgb[2];
    //Формирование палитры
    unsigned int factor = 255 / 1;
    unsigned int index = 0;

    for (int i=0;i<256;i+=factor)
    {
        rgb[index].rgbBlue=i;
        rgb[index].rgbGreen=i;
        rgb[index].rgbRed=i;
        rgb[index].rgbReserved=0;
        index++;
    }

    ofstream file (nameOfColor2File,ios::binary|ios::out|ios_base::trunc);
    if (!file.bad()){
        file.write((const char *)&bmfh,sizeof(bmfh));
        file.write((const char *)&bmV5h,sizeof(bmV5h));
        file.write((const char *)rgb,sizeof(rgb));
        file.write((const char *)BitmapFull,lengthOfReadFile);
        file.close();
    }
}

void MainWindow::createNewBMPcolor16(string path)
{
    //заглушка
    //path = "COLOR16.MTX";

    readRawMatrix(path);

    BITMAPFILEHEADER bmfh = createFileHeader(16);
    BITMAPV5HEADER bmV5h = createPV5Header(4);

    RGBQUAD rgb[16];
    //Формирование палитры
    unsigned int factor = 255 / 16;
    unsigned int index = 0;

    for (int i=0;i<256;i+=factor)
    {
        rgb[index].rgbBlue=i;
        rgb[index].rgbGreen=i;
        rgb[index].rgbRed=i;
        rgb[index].rgbReserved=0;
        index++;
    }

    ofstream file (nameOfColor16File,ios::binary|ios::out|ios_base::trunc);
    if (!file.bad()){
        file.write((const char *)&bmfh,sizeof(bmfh));
        file.write((const char *)&bmV5h,sizeof(bmV5h));
        file.write((const char *)rgb,sizeof(rgb));
        file.write((const char *)BitmapFull,lengthOfReadFile);
        file.close();
    }
}

void MainWindow::createNewBMPcolor256(string path)
{
    //заглушка
    //path = "COLOR256.MTX";

    readRawMatrix(path);

    BITMAPFILEHEADER bmfh = createFileHeader(256);
    BITMAPV5HEADER bmV5h = createPV5Header(8);

    RGBQUAD rgb[256];
    //Формирование палитры
    unsigned int factor = 255 / 255;
    unsigned int index = 0;

    for (int i=0;i<256;i+=factor)
    {
        rgb[index].rgbBlue=i;
        rgb[index].rgbGreen=i;
        rgb[index].rgbRed=256-i;
        rgb[index].rgbReserved=0;
        index++;
    }

    ofstream file (nameOfColor256File,ios::binary|ios::out|ios_base::trunc);
    if (!file.bad()){
        file.write((const char *)&bmfh,sizeof(bmfh));
        file.write((const char *)&bmV5h,sizeof(bmV5h));
        file.write((const char *)rgb,sizeof(rgb));
        file.write((const char *)BitmapFull,lengthOfReadFile);
        file.close();
    }
}

void MainWindow::createNewBMPcolor16m(string path)
{
    //заглушка
    //path = "COLOR16M.MTX";

    readRawMatrix(path);

    BITMAPFILEHEADER bmfh = createFileHeader(0);
    BITMAPV5HEADER bmV5h = createPV5Header(24);

    ofstream file (nameOfColor16mFile,ios::binary|ios::out|ios_base::trunc);
    if (!file.bad()){
        file.write((const char *)&bmfh,sizeof(bmfh));
        file.write((const char *)&bmV5h,sizeof(bmV5h));
        file.write((const char *)BitmapFull,lengthOfReadFile);
        file.close();
    }
}

void MainWindow::openAndPrintBMP()
{
    openAndPrintBMPcolor2();
    openAndPrintBMPcolor16();
    openAndPrintBMPcolor256();
    openAndPrintBMPcolor16m();
}


void MainWindow::openAndPrintBMPcolor2()
{
    QPixmap pm(QString().fromStdString(nameOfColor2File)); // <- path to image file
    ui->labelColor2->setPixmap(pm);
    ui->labelColor2->setScaledContents(true);
}

void MainWindow::openAndPrintBMPcolor16()
{
    QPixmap pm(QString().fromStdString(nameOfColor16File)); // <- path to image file
    ui->labelColor16->setPixmap(pm);
    ui->labelColor16->setScaledContents(true);
}

void MainWindow::openAndPrintBMPcolor256()
{
    QPixmap pm(QString().fromStdString(nameOfColor256File)); // <- path to image file
    ui->labelColor256->setPixmap(pm);
    ui->labelColor256->setScaledContents(true);
}

void MainWindow::openAndPrintBMPcolor16m()
{
    QPixmap pm(QString().fromStdString(nameOfColor16mFile)); // <- path to image file
    ui->labelColor16m->setPixmap(pm);
    ui->labelColor16m->setScaledContents(true);
}
